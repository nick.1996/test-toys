import Vue from 'vue'
import VueRouter from 'vue-router'

const index = () => import(/* webpackChunkName: "index" */ '@/views/index.vue')
const info = () => import(/* webpackChunkName: "info" */ '@/views/info.vue')
const mapToys = () => import(/* webpackChunkName: "mapToys" */ '@/views/mapToys.vue')
const work = () => import(/* webpackChunkName: "mapToys" */ '@/views/work.vue')
const basket = () => import(/* webpackChunkName: "Basket" */ '@/views/basket.vue')
const covid19 = () => import(/* webpackChunkName: "covid19" */ '@/views/covid_19.vue')
const todoList = () => import(/* webpackChunkName: "todoList" */ '@/views/todoList.vue')

Vue.use(VueRouter)

const router = new VueRouter({
  linkActiveClass: 'activebtn',
  mode: 'history',
  routes: [
    {
      path: '/',
      component: index,
      meta: { title: 'Info' },
      redirect: '/info',
      children: [
        {
          path: '/info',
          name: 'info',
          meta: { title: 'VueBasic' },
          component: info
        },
        {
          path: '/mapToys',
          name: 'mapToys',
          meta: { title: 'Map' },
          component: mapToys
        },
        {
          path: '/basket',
          name: 'basket',
          meta: { title: 'Basket' },
          component: basket
        },
        {
          path: '/covid19',
          name: 'covid_19',
          meta: { title: 'covid_19' },
          component: covid19
        },
        {
          path: '/work',
          name: 'HomeWork',
          meta: { title: 'HomeWork' },
          component: work
        },
        {
          path: '/todoList',
          name: 'TodoList',
          meta: { title: 'TodoList' },
          component: todoList
        }
      ]
    }
  ]
})

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  return originalPush.call(this, location).catch((err) => err)
}

router.beforeEach((to, from, next) => {
  window.document.title = `${to.meta.title} | Nick-Web`
  next()
})

export default router
